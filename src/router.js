import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";
import Details from "./views/Details.vue";
import Scores from "./views/Scores.vue";
import Hystoric from "./views/Hystoric.vue";
import Tickets from "./views/Tickets.vue";
import Branches from "./views/Branches.vue";
import CheckList from "./views/CheckList.vue";
import Metas from "./views/Metas.vue";

Vue.use(Router);

let router = new Router({
  mode: process.env.NODE_ENV == "production" ? "history" : "hash",
  base: process.env.BASE_URL,
  routes: [{
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        guest: true
      }
    },
    {
      path: "/",
      name: "tickets",
      component: Tickets,
      meta: {
        requiresAuth: true,
        is_supervisor: true,
        is_admin: true,
        is_director: true,
        is_operator: true
      }
    },

    {
      path: "/details/:id",
      name: "details",
      component: Details,
      meta: {
        requiresAuth: true,
        is_supervisor: true,
        is_admin: true,
        is_director: true,
        is_operator: true
      },
      params: true
    },
    {
      path: "/scores",
      name: "scores",
      component: Scores,
      meta: {
        requiresAuth: true,
        is_supervisor: true,
        is_admin: true,
        is_director: true
      }
    },
    {
      path: "/metas",
      name: "metas",
      component: Metas,
      meta: {
        requiresAuth: true,
        is_supervisor: true
      }
    },
    {
      path: "/hystoric",
      name: "hystoric",
      component: Hystoric,
      meta: {
        requiresAuth: true,
        is_admin: true,
        is_supervisor: true
      }
    },
    {
      path: "/branches",
      name: "branches",
      component: Branches,
      params: true,
      meta: {
        requiresAuth: true,
        is_tienda: true,
        is_supervisor: true,
        is_director: true
      }
    },
    {
      path: "/checklist",
      name: "checklist",
      component: CheckList,
      params: true,
      meta: {
        requiresAuth: true,
        is_tienda: true,
        is_supervisor: true,
        is_director: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  const user = JSON.parse(localStorage.getItem("userInfo"));
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({
        path: "/login"
      });
    }
    if (to.matched.some(record => record.meta.is_admin)) {
      if (user.role.includes("admin")) next();
    }
    if (to.matched.some(record => record.meta.is_director)) {
      if (user.role.includes("director")) next();
    }
    if (to.matched.some(record => record.meta.is_supervisor)) {
      if (user.role.includes("supervisor")) next();
    }
    if (to.matched.some(record => record.meta.is_operator)) {
      if (user.role.includes("operadores")) next();
    }
    if (to.matched.some(record => record.meta.is_tienda)) {
      if (user.role.includes("tienda")) next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;
