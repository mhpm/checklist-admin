import loadingComp from "../components/shared/Loading";
import Spinner from '../components/shared/Spinner'

const Loading = {
  install(Vue, options) {
    Vue.component(loadingComp);
    Vue.component('spinner', Spinner)

    // global function for show or hide modal loading
    Vue.prototype.$showLoading = function (status) {
      this.$bus.$emit("showLoading", status);
    };

    //code for auto instance the loading component
    let ComponentClass = Vue.extend(loadingComp)
    var instance = new ComponentClass()
    instance.$mount()

    Vue.mixin({
      mounted() {
        //code for auto instance the loading component only on root app
        this.$root.$el.appendChild(instance.$el)
      },
    })
  }
};

export default Loading;
