class AuthService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}oauth/token`;
  }

  Login(user, password) {
    let self = this;

    // Credential only for granvia client
    let data = {
      client_secret: "JmW1CNwYC5sO2q0j4adR46wFxJJbdIw8z40ms1LR",
      client_id: 2,
      grant_type: "password",
      username: user,
      password: password
    };

    return self.axios.post(`${self.baseUrl}`, data);
  }

  setToken(token, expiration) {
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expiration);
  }

  getToken() {
    let token = localStorage.getItem("token");
    let expiration = localStorage.getItem("expiration");

    if (!token || !expiration) return null;

    if (!Date.now() > parseInt(expiration)) {
      this.destroyToken();
      return null;
    } else {
      return token;
    }
  }

  destroyToken() {
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
  }

  isAuthenticated() {
    return !!this.getToken();
  }
}

export default AuthService;
