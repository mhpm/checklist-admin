/* eslint-disable */
import Vue from 'vue'
import Auth from '../components/auth/Auth.js'
import Axios from 'axios'
import ticketService from './TicketService'
import authService from './AuthService'
import userService from './UserService'
import noteService from './NoteService'
import scoreService from './ScoreService'
import checklistService from './ChecklistService'
import metaService from './MetaService'

Vue.use(Auth)

// API
var apiUrl = process.env.VUE_APP_URL_DEMO

//validation for chancge API client
if (window.location.hostname == 'gvchecklist.orkestra.mx')
  apiUrl = `https://apigv.gvchecklist.orkestra.mx/`

// Axios configuration
Axios.defaults.headers.common.Accept = 'application/json'
Axios.defaults.headers.post['Content-Type'] = 'application/json'
if (localStorage.getItem('token') != null) {
  Axios.defaults.headers.common['Authorization'] =
    'Bearer ' + Vue.auth.getToken()
}

export default {
  ticketService: new ticketService(Axios, apiUrl),
  authService: new authService(Axios, apiUrl),
  userService: new userService(Axios, apiUrl),
  noteService: new noteService(Axios, apiUrl),
  scoreService: new scoreService(Axios, apiUrl),
  checklistService: new checklistService(Axios, apiUrl),
  metaService: new metaService(Axios, apiUrl),
  apiUrl,
}
