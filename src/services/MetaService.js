class NoteService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api/dashboard`;
    this.apiUrl = apiUrl;
  }

  getAll(week) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/?weekOfYear=${week}`);
  }

  getHistoric() {
    let self = this;
    return self.axios.get(`${self.baseUrl}/historic`);
  }
}

export default NoteService;
