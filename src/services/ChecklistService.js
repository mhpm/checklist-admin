class ChecklistService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api`;
    this.apiUrl = apiUrl;
  }

  getBranches() {
    let self = this;
    return self.axios.get(`${self.baseUrl}/branchOffices`);
  }

  getBranchesBySearch(search) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/branchOffices?search=${search}`);
  }

  getQuestionsByCategories() {
    let self = this;
    return self.axios.get(`${self.baseUrl}/categories/questions`);
  }

  saveData(dataRequest) {
    let self = this;
    return self.axios.post(`${self.baseUrl}/answered`, dataRequest);
  }

  cancelChecklist() {
    let self = this;
    return self.axios.delete(`${self.baseUrl}/answered/delete`);
  }

  finishChecklist(userId) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/users/${userId}/finished`);
  }
}

export default ChecklistService;
