class UserService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api/user`;
  }

  getUser() {
    let self = this;
    return self.axios.get(`${self.baseUrl}`);
  }
}

export default UserService;
