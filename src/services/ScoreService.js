class ScoreService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api/scores`;
    this.apiUrl = apiUrl;
  }

  getByGroup(group) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/?groupby=${group}`);
  }

  getFilters() {
    let self = this;
    return self.axios.get(`${self.baseUrl}/filters`);
  }

  getScoresFiltered(query, groupby) {
    let self = this;
    return self.axios.get(`${self.baseUrl}${query}groupby=${groupby}`);
  }

  exportFile(query) {
    let self = this;
    return `${self.apiUrl}api/scores/export${query}`;
  }
}

export default ScoreService;
