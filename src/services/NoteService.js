class NoteService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api/notes`;
    this.apiUrl = apiUrl;
  }

  Save(dataRequest) {
    let self = this;
    return self.axios.post(`${self.baseUrl}`, dataRequest);
  }

  Edit(id, dataRequest) {
    let self = this;
    return self.axios.put(`${self.baseUrl}/${id}`, dataRequest);
  }

  Delete(id) {
    let self = this;
    return self.axios.delete(`${self.baseUrl}/${id}`);
  }

  getNotesByPage(id, page) {
    let self = this;
    return self.axios.get(`${self.apiUrl}api/tickets/${id}/notes?page=${page}`);
  }
}

export default NoteService;
