class TicketService {
  axios;
  baseUrl;

  constructor(axios, apiUrl) {
    this.axios = axios;
    this.baseUrl = `${apiUrl}api/tickets`;
    this.apiUrl = apiUrl;
  }

  getAll() {
    let self = this;
    return self.axios.get(`${self.baseUrl}`);
  }

  getTicketsByPage(query) {
    let self = this;
    return self.axios.get(`${self.baseUrl}` + query);
  }

  getTicketsDetails(id) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/` + id + "/edit");
  }

  getFilters() {
    let self = this;
    return self.axios.get(`${self.baseUrl}/filters`);
  }

  getTicketsFiltered(filters) {
    let self = this;
    return self.axios.get(`${self.baseUrl}${filters}`);
  }

  getLogsNotes(id) {
    let self = this;
    return self.axios.get(`${self.baseUrl}/${id}/notes`);
  }

  saveTicketLog(id, dataRequest) {
    let self = this;
    return self.axios.patch(`${self.baseUrl}/${id}`, dataRequest);
  }
}

export default TicketService;
