import Vue from "vue";
import Vuex from "vuex";
import services from "./services/services";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    services,
    userInfo: localStorage.userInfo ? JSON.parse(localStorage.getItem("userInfo")) : false,
    categories: localStorage.categories ? JSON.parse(localStorage.categories) : false
  },
  mutations: {
    SET_USER_INFO(state, data) {
      state.userInfo = data;
    },
    SET_CATEGORIES(state, data) {
      state.categories = data;
    }
  },
  getters: {
    isLogged(state) {
      return !!state.userInfo;
    },
    getUserInfo(state) {
      return state.userInfo;
    },
    getCategories(state) {
      return state.categories;
    }
  },
  actions: {
    SetUserInfo(state, data) {
      state.commit("SET_USER_INFO", data);
    },
    SetCategories(state, data) {
      state.commit("SET_CATEGORIES", data);
    }
  }
});
