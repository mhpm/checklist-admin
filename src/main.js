import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Loading from './plugin/Loading'
import ImageUploader from 'vue-image-upload-resize'
import VueOffline from 'vue-offline'
import EventBus from './plugin/EventBus'
import VuePaginate from 'vue-paginate'
import VueGeolocation from 'vue-browser-geolocation'
import 'animate.css'

import $ from "jquery";
import 'bootstrap'

//GLOBAL UTILS
Vue.mixin({
  data() {
    return {
      clientImg: 'checkapp.png' //default logo image
    }
  },
  mounted() {
    // function for close side menu automatic
    $(document).ready(function () {
      $('#check').prop('checked', false)
      $('#bg').hide()
      $('html, body').css({
        overflow: 'auto',
        height: '100%'
      })
    })

    //validation for chancge logo client
    if (window.location.hostname == 'gvchecklist.orkestra.mx')
      this.clientImg = 'gv.png'
  },
  methods: {
    resetStorageData() {
      //console.log('resetStorageData')
      this.$store.getters.getUserInfo.answereds = false

      if (!this.$store.getters.getUserInfo.role.includes('tienda'))
        this.$store.getters.getUserInfo.branchSelected = false

      this.$store.dispatch('SetUserInfo', this.$store.getters.getUserInfo)
      this.$offlineStorage.set('userInfo', this.$store.getters.getUserInfo)
      this.$store.dispatch('SetCategories', this.$store.getters.getCategories)
      this.$offlineStorage.set('categories', this.$store.getters.getCategories)
    },
    hasRole(role) {
      return this.$store.getters.getUserInfo.role.includes(role);
    }
  }
})

Vue.use(VueGeolocation)
Vue.use(EventBus)
Vue.use(VuePaginate)
Vue.use(VueOffline)
Vue.use(ImageUploader)
Vue.use(Loading)
global.$ = $

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
